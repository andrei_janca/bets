import os
import sys
import glob

rankings_filelist = glob.glob(os.path.join(sys.argv[1], '*[!~]'))
outdir = os.path.join('csv_data', 'rankings')

if not os.path.exists(outdir):
    os.makedirs(outdir)

for rankings_file in rankings_filelist:
    filename = rankings_file.split('/')[-1]
    filepath = os.path.join(outdir, filename)

    with open(filepath, 'w') as g:
        g.write('Team,Ranking1,Ranking2,Ranking3,Ranking4\n')

        with open(rankings_file, 'r') as f:
            for i, line in enumerate(f):
                line = line.strip()

                if line == '': continue

                tokens = list(map(str.strip, line.split('\t')))
                g.write('{},{},{},{},{}\n'.format(tokens[1][3:],
                                                    tokens[6],
                                                    tokens[9],
                                                    tokens[11],
                                                    tokens[13]))
