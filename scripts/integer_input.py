## For training and validation sets: from .data files to .csv (where countries are indexed)
import os
import sys

with open(sys.argv[1],encoding="UTF-8") as f:
    content = f.readlines()

content = [x.strip() for x in content]

with open("../raw_data/rankings.txt",encoding="UTF-8") as f:
    rankings = f.readlines()

countries = []
coeficients = []
for x in rankings:
    tokens = x.strip().split('=')
    countries.append(tokens[0].strip())
    coeficients.append(tokens[1].strip())

f = open(sys.argv[2], "wt", encoding="UTF-8")

f.write('Team1Ranking1,Team1Ranking2,Team1Ranking3,Team1Ranking4,Team1Ranking5,Team1Ranking6,Team1Value,Team2Ranking1,Team2Ranking2,Team2Ranking3,Team2Ranking4,Team2Ranking5,Team2Ranking6,Team2Value,Score1,Score2\n')

for x in content:
    result = x.split(",")
    p1 = coeficients[countries.index(result[0])]
    p2 = coeficients[countries.index(result[3])]
        
    f.write(p1 + ',' + p2 + ',' + result[1] + ',' + result[2] + '\n')
    f.write(p2 + ',' + p1 + ','  + result[2] + ',' + result[1] + '\n')

f.close()
