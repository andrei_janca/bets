import os
import sys

with open(sys.argv[1],encoding="UTF-8") as f:
    content = f.readlines()

content = [x.strip() for x in content]

f = open(sys.argv[2], "wt", encoding="UTF-8")
for line in content:
    tokens = line.split(',')
    nr = len(tokens)
    f.write(tokens[0].strip() + '=')
    for i in range(1, nr - 1):
        f.write(tokens[i].strip() + ',')
    f.write(tokens[nr - 1].strip() + '\n')
f.close()
            
        
