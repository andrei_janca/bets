## from text file from fifa.com to .data 
import os
import sys

with open(sys.argv[1],encoding="UTF-8") as f:
    content = f.readlines()

with open("../raw_data/participants.txt",encoding="UTF-8") as f:
    participants = f.readlines()

participants = [x.strip() for x in participants]
print(participants)

f = open(sys.argv[2], "wt", encoding="UTF-8")
print(len(content))
i = 0
while i < len(content):
    if content[i].startswith("FT"):
        country1 = content[i + 1].strip()
        country2 = content[i + 3].strip()
        score = content[i + 2].split("-")
        datetokens = content[i + 4].split(' ')
        if (len(datetokens) == 2):
            date = datetokens[1].strip()
#        if country1 in participants or country2 in participants:
        f.write(country1 + ',' + score[0].strip() + ',' + score[1].strip() + ',' + country2 + ',' + date + '\n')
        i = i + 4
    else:
        i = i + 1
f.close()
