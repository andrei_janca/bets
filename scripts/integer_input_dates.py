## For training and validation sets: from .data files to .csv (where countries are indexed)
import os
import sys
import glob
import time

with open(sys.argv[1],encoding="UTF-8") as f:
    content = f.readlines()

content = [x.strip() for x in content]

filelist = glob.glob("../csv_data/rankings/*")

def get_date_from_filename(filename):
    date_str = filename.split('/')[3]
    date_fmt = date_str[:2] + '/' + date_str[2:4] + '/' + date_str[4:]
    return time.strptime(date_fmt, "%d/%m/%Y") 

print(filelist)
ranking_files = dict()
dates = []
for x in filelist:
    dates.append(get_date_from_filename(x))
    ranking_files[get_date_from_filename(x)] = x

dates.sort()

f = open(sys.argv[2], "wt", encoding="UTF-8")

for i in range(0, 4 * len(ranking_files)):
    f.write('Team1Ranking{},'.format(i))
f.write('Value1,')
for i in range(0, 4 * len(ranking_files)):
    f.write('Team2Ranking{},'.format(i))
f.write('Value2,Score1,Score2\n')


for x in content:
    result = x.split(",")
    print(x.encode('utf-8'))
    #try:
    #    date = time.strptime(result[4], "%d/%m/%Y")
    #except:
    #    pass

    #i = 0
    #while i < len(dates):
    #    if dates[i] > date:
    #        break
    #    i = i + 1
    final1 = ""
    final2 = ""
    for x in ranking_files:
        try:
            with open(x, encoding="UTF-8") as g:
                rankings = g.readlines()

            countries = []
            coeficients = []
            for x in rankings:
                tokens = x.strip().split(',')
                countries.append(tokens[0].strip())
                coeficients.append(tokens[1].strip() + ',' + tokens[2].strip() +',' + tokens[3].strip() + ',' + tokens[4].strip() + ',')
                
            p1 = coeficients[countries.index(result[0])]
            p2 = coeficients[countries.index(result[3])]
            final1 += p1
            final2 += p2
        except:
            with open("../csv_data/rankings/07062018",encoding="UTF-8") as g:
                rankings = g.readlines()

            countries = []
            coeficients = []
            for x in rankings:
                tokens = x.strip().split(',')
                countries.append(tokens[0].strip())
                coeficients.append(tokens[1].strip() + ',' + tokens[2].strip() +',' + tokens[3].strip() + ',' + tokens[4].strip() + ',')
                
            p1 = coeficients[countries.index(result[0])]
            p2 = coeficients[countries.index(result[3])]
            final1 += p1
            final2 += p2

    with open("../raw_data/rankings.txt",encoding="UTF-8") as g:
        rankings = g.readlines()

    countries = []
    prices = []
    for x in rankings:
        tokens = x.strip().split('=')
        countries.append(tokens[0].strip())
        values = tokens[1].strip().split(',')
        prices.append(values[len(values)-1])
        
    price1 = prices[countries.index(result[0])]
    price2 = prices[countries.index(result[3])]

#    print('count = ' + str(4 * len(ranking_files)))
#    outcome = 1 if int(result[1]) > int(result[2]) else 0 if int(result[1]) == int(result[2]) else 2    
    f.write(final1  + price1 + ',' + final2 + price2 + ',' + result[1] + ',' + result[2] + '\n')
    f.write(final2 +  price2 + ',' + final1 + price1 + ',' + result[2] + ',' + result[1] + '\n')

f.close()
